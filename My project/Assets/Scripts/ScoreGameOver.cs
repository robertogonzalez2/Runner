using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreGameOver : MonoBehaviour
{
    [SerializeField]
    public DatosScore scoregameover;
    private TMPro.TextMeshProUGUI textscore;
    
    //Aquest Script el que fa es passarli les dades del scriptable object de Score a la escena de GameOver
    void Start()
    {
        textscore = GetComponent<TMPro.TextMeshProUGUI>();
        textscore.text = textscore.text + scoregameover.scoretotal;
    }

    
    void Update()
    {
        
    }
}
