using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject objectenou;
    public Velocidad vel_dificultad;
    void Start()
    {
        StartCoroutine(EnemicSpawner());
    }

    
    void Update()
    {
        
    }
    // Aquesta corrutina invoca un cactus cada 2 a 4 segns i posa el cactus a una posicio aleatoria
    IEnumerator EnemicSpawner()
    {
        while (true)
        {
        yield return new WaitForSeconds(Random.Range(2, 4));

        
        GameObject cactus = Instantiate(objectenou);

        float posicion = Random.Range(1,11);
        cactus.transform.position = new Vector2(this.transform.position.x + posicion, this.transform.position.y);

        cactus.GetComponent<Rigidbody2D>().velocity = new Vector2(vel_dificultad.velocidad, 0);
        }
    }

}
