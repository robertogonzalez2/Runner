using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    public DatosScore datosScore;
    private TMPro.TextMeshProUGUI textscore;
    public Velocidad vel_dificultad;
    // Start is called before the first frame z
    void Start()
    {
        textscore = GetComponent<TMPro.TextMeshProUGUI>();
        StartCoroutine(aumentar_sacore());
        datosScore.scoretotal = 0;
    }

    
    void Update()
    {
        
    }
    // Cada 0.2 segons s'agrega 1 punt al score i s'incrementa la velocitat.
    IEnumerator aumentar_sacore(){
        while(true){
            yield return new WaitForSeconds(0.2f);

            datosScore.scoretotal = datosScore.scoretotal + 1;
            textscore.text = "Score: " + datosScore.scoretotal;
            vel_dificultad.velocidad = -10 + (datosScore.scoretotal / -30);
        }

    }
}
